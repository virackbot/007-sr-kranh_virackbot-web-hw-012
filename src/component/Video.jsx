import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function Video(props) {
  const opt = props.match.params.id;
  const category = props.match.params.category;
  const animation = (
    <div>
      <h1>Animation category</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/action">
            Action
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/romance">
            Romance
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/comedy">
            Comedy
          </Link>
        </li>
      </ul>
    </div>
  );
  const movie = (
    <div>
      <h1>Movie category</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/adventure">
            Adventure
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/comedy">
            Comedy
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/crime">
            Crime
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/documentary">
            Documentary
          </Link>
        </li>
      </ul>
    </div>
  );

  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/animation">
            Animation
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie">
            Movies
          </Link>
        </li>
      </ul>
      {opt === "animation" ? animation : null}
      {opt === "movie" ? movie : null}
      {category === undefined ? (
        <h3>please select a topic</h3>
      ) : (
        <h3>{category}</h3>
      )}
    </div>
  );
}

export default Video;
