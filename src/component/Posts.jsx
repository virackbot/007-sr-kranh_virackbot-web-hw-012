import React from "react";

function Posts(props) {
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
        <div className="box rounded mt-4 bg-light p-5 h-75">
      <h1>This content from post {props.match.params.id}</h1>
    </div>
    </div>
  );
}

export default Posts;
