import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";

function Welcome({ isAuth, history }) {
  if (isAuth === false) {
    history.push("/auth");
  }
  console.log(isAuth);
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <h1>Welcome</h1>
    </div>
  );
}

export default Welcome;
